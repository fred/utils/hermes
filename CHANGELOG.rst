ChangeLog
=========


Unreleased
----------

6.0.0 (2024-09-25)
------------------

* Use ``pydantic`` v2 API (#32).

5.3.0 (2024-09-25)
------------------

* Upgrade to ``fred-frgal`` ~= 3.15 (#30).
* Allow iterable as email recipient (#22).
* Fix annotations.
* Use ``ruff``
* Update project setup.

5.2.0 (2023-11-01)
------------------

* Add pydantic v2 support (#29)
  * ``hermes`` is now compatible with both ``pydantic`` v1 and ``pydantic`` v2
* Update supported python versions (drop 3.7, add 3.12)

5.1.0 (2023-08-31)
------------------

* Pass ``**kwargs`` to ``AsyncGrpcClient`` init (#26)

5.0.0 (2023-04-03)
------------------

* **[BREAKING]** Upgrade messenger API version to 1.3 (#18)
* **[BREAKING]** Rename ``country_code`` to ``country`` (#18)
* **[BREAKING]** Change message references in API to strings (#18)
* **[BREAKING]** Remove ``ReferenceType`` enum (#20)
* **[BREAKING]** Fix frgal deprecations (#23)

  * ``MessengerDecoder`` now uses ``decode_empty_string_none=False``
    and ``decode_unset_messages=False``

* Add ``batch_send`` method (#18)
* Add ``TransportMethod`` enum (#19)
* Add ``body_template`` and ``context`` information to Letter (#18)
* Use ``BaseModel`` from ``fred-types`` (#21)
* Expose ``Reference`` to ``__init__``
* Expose ``Address`` to ``__init__``
* Update setup and reformat with Black

4.1.0 (2022-06-02)
------------------

* Add ``OTHER`` item to reference enum (#16).

4.0.0 (2022-03-21)
------------------

* **[BREAKING]** Change secretary_client to a private Message attribute
  (it remains public for messenger client classes)

3.0.1 (2022-03-18)
------------------

* Fix decoding of email attachments

3.0.0 (2022-03-10)
------------------

* **[BREAKING]** Use pydantic models
* **[BREAKING]** Split MessengerClient to individual async services
* **[BREAKING]** Interpret references and file identifier as strings
* **[BREAKING]** Drop custom Struct decoder

* Add async messenger client
* Enable mypy warnings
* Replace type hints in comments with inline
* Update python versions (drop 3.6, add 3.9 and 3.10)

2.0.0 (2021-09-20)
------------------

* Add structure for registry object references
* Add references to list and send methods
* Remove deprecated MessengeClient methods ``get``, ``send`` and ``list``

1.0.1 (2020-12-14)
------------------

* Fix Letter.from_dict method mutating its argument

1.0.0 (2020-12-11)
------------------

* Remove support for Python 3.5
* Add support for letters

0.5.1 (2020-11-23)
------------------

* Fix MessageEnvelope.from_dict mutating its argument

0.5.0 (2020-11-18)
------------------

* Add generic types Message and MessageEnvelope
* Add support for SMS
* Add support for message references
* Update README and CHANGELOG to conform to CZ.NIC documentation style guide

0.4.0 (2020-10-22)
------------------

* Upgrade fred-api-messenger dependency to 1.0.*
* Add message type to the Email object
* Add list filter by message types

0.3.0 (2020-08-25)
------------------

* Catch render errors in to_dict method

0.2.1 (2020-08-25)
------------------

* Fix list method to pass along secretary client

0.2.0 (2020-08-20)
------------------

* Upgrade fred-api-messenger dependency to 0.4.*
* Add to_dict method for Email and EmailEnvelope serialization

0.1.0 (2020-08-11)
------------------

* Add Email and EmailEnvelope objects
* Add send, get and list methods calling messenger API
* Add subject, body and body_html Email properties calling secretary API
