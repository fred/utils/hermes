"""High level API for letters."""

from typing import Any, Dict, Optional
from uuid import UUID

from fred_types import BaseModel

from .message import Message, MessageEnvelope


class Address(BaseModel):
    """Letter recipient address."""

    name: str = ""
    organization: str = ""
    street: str = ""
    city: str = ""
    state_or_province: str = ""
    postal_code: str = ""
    country: str = ""


class Letter(Message):
    """Provide a structure for letter message."""

    recipient: Address
    file: str
    body_template: Optional[str] = None
    body_template_uuid: Optional[UUID] = None
    context: Dict[str, Any] = {}

    render_fields = ()


class LetterEnvelope(MessageEnvelope[Letter]):
    """Provide a structure for letter envelope."""

    archive_rendered: bool = False
