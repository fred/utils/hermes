"""Messenger gRPC grpc-messenger-client."""

from abc import ABC, abstractmethod
from datetime import datetime
from typing import (
    Any,
    AsyncGenerator,
    Dict,
    Generic,
    Iterable,
    List,
    Optional,
    Sequence,
    Type,
    TypeVar,
    Union,
    cast,
)
from uuid import UUID

import grpc
from fred_api.messenger.common_types_pb2 import Reference as MessengerReference, Uuid
from fred_api.messenger.email_pb2 import Email as EmailMessage
from fred_api.messenger.letter_pb2 import Address as MessengerAddress, Letter as LetterMessage
from fred_api.messenger.service_email_grpc_pb2 import (
    GetEmailRequest,
    ListEmailRequest,
    SendEmailRequest,
)
from fred_api.messenger.service_email_grpc_pb2_grpc import EmailMessengerStub
from fred_api.messenger.service_letter_grpc_pb2 import (
    GetLetterRequest,
    ListLetterRequest,
    SendLetterRequest,
)
from fred_api.messenger.service_letter_grpc_pb2_grpc import LetterMessengerStub
from fred_api.messenger.service_sms_grpc_pb2 import GetSmsRequest, ListSmsRequest, SendSmsRequest
from fred_api.messenger.service_sms_grpc_pb2_grpc import SmsMessengerStub
from fred_api.messenger.sms_pb2 import Sms as SmsMessage
from frgal import GrpcDecoder
from frgal.aio import AsyncGrpcClient
from typist import SecretaryClient

from .email import Email, EmailEnvelope
from .letter import Letter, LetterEnvelope
from .message import Message, MessageEnvelope, Reference
from .sms import Sms, SmsEnvelope
from .utils import AbcAttribute

E = TypeVar("E", bound=MessageEnvelope)
M = TypeVar("M", bound=Message)
GetRequest = TypeVar("GetRequest", bound=Union[GetEmailRequest, GetLetterRequest, GetSmsRequest])
SendRequest = TypeVar(
    "SendRequest", bound=Union[SendEmailRequest, SendLetterRequest, SendSmsRequest]
)
ListRequest = TypeVar(
    "ListRequest", bound=Union[ListEmailRequest, ListLetterRequest, ListSmsRequest]
)


class HermesError(Exception):
    """Base class for Hermes related errors."""


class NoMessage(HermesError):
    """Raised when no message matching the UUID is found."""


class SendFailed(HermesError):
    """Raised when sending of the message failed."""


class MessengerDecoder(GrpcDecoder):
    """Messenger gRPC decoder."""

    decode_empty_string_none = False
    decode_unset_messages = False

    def __init__(self) -> None:
        """Initialize the decoder object by registering custom decoders."""
        super().__init__()
        self.set_decoder(EmailMessage, self._decode_email)
        self.set_decoder(LetterMessage, self._decode_letter)
        self.set_decoder(SmsMessage, self._decode_sms)
        self.set_decoder(MessengerReference, self._decode_reference)
        self.set_decoder(Uuid, self._decode_uuid)

    def _decode_email(self, value: EmailMessage) -> Dict[str, Any]:
        """Decode email.

        UUID is decoded to string.
        """
        # We take attachments out of the message to avoid UUID interpretation
        copy = EmailMessage()
        copy.CopyFrom(value)
        del copy.attachments[:]

        email = self._decode_message(copy)
        email["attachments"] = [str(attachment.value) for attachment in value.attachments]
        email["context"] = email["context"] or {}
        email["type"] = email["type"] or None
        email["subject_template"] = email["subject_template"] or None
        email["body_template"] = email["body_template"] or None
        email["body_template_html"] = email["body_template_html"] or None
        return cast(Dict[str, Any], email)

    def _decode_letter(self, value: LetterMessage) -> Dict[str, Any]:
        """Decode letter.

        UUID is decoded to string.
        """
        # We take file out of the message to avoid UUID interpretation
        copy = LetterMessage()
        copy.CopyFrom(value)
        copy.file.value = ""

        letter = self._decode_message(copy)
        letter["file"] = value.file.value
        letter["context"] = letter["context"] or {}
        letter["type"] = letter["type"] or None
        letter["body_template"] = letter["body_template"] or None
        return cast(Dict[str, Any], letter)

    def _decode_sms(self, value: SmsMessage) -> Dict[str, Any]:
        """Decode SMS."""
        # We take file out of the message to avoid UUID interpretation
        copy = SmsMessage()
        copy.CopyFrom(value)

        sms = self._decode_message(copy)
        sms["context"] = sms["context"] or {}
        sms["type"] = sms["type"] or None
        sms["body_template"] = sms["body_template"] or None
        return cast(Dict[str, Any], sms)

    def _decode_reference(self, value: MessengerReference) -> Reference:
        """Decode Reference."""
        # 'old_type' field is no more used
        return Reference(type=value.type, value=value.value)

    def _decode_uuid(self, value: Uuid) -> Optional[UUID]:
        """Decode Uuid."""
        if not value.value:
            return None
        return UUID(value.value)


class MessengerClientBase(
    ABC, AsyncGrpcClient, Generic[E, M, GetRequest, SendRequest, ListRequest]
):
    """Base class for messenger clients."""

    envelope_cls: Type[E] = AbcAttribute
    get_request_cls: Type[GetRequest] = AbcAttribute
    list_request_cls: Type[ListRequest] = AbcAttribute

    decoder_cls = MessengerDecoder

    def __init__(
        self,
        netloc: str,
        credentials: Optional[grpc.ChannelCredentials] = None,
        *,
        secretary_client: Optional[SecretaryClient] = None,
        **kwargs: Any,
    ):
        """Initialize Logger GRPC backend grpc-messenger-client instance.

        Args:
            netloc: Network location of a gRPC server.
            credentials: Credentials for a secure channel connection.
                If None, insecure channel is used.
            secretary_client: Instance of django-secretary REST API client.
            kwargs: Other kwargs are passed to AsyncGrpcClient.
        """
        super().__init__(netloc, credentials=credentials, **kwargs)
        self.secretary_client = secretary_client

    @abstractmethod
    def make_send_request(
        self, message: M, *, archive: bool, references: Sequence[Reference]
    ) -> SendRequest:
        """Make send request."""

    async def get(self, uid: str) -> E:
        """Send gRPC request to get message.

        Args:
            uid: Unique message identification.

        Raises:
            NoMessage: When no message matching uid is found.
        """
        request = self.get_request_cls()
        request.uid.value = uid
        try:
            envelope_data = await self.call("get", request)
        except grpc.RpcError as error:
            if error.code() == grpc.StatusCode.NOT_FOUND:
                raise NoMessage("Message {} not found".format(uid)) from error
            raise
        envelope = self.envelope_cls(**envelope_data)
        envelope.message._secretary_client = self.secretary_client
        return envelope

    async def send(
        self, message: M, *, archive: bool = True, references: Optional[Sequence[Reference]] = None
    ) -> str:
        """Send gRPC request to send message request.

        Args:
            message: Message structure to be sent
            archive: Whether the message should be archived
            references: List of registry object references.

        Raises:
            SendFailed: When sending of the message fails.
        """
        request = self.make_send_request(
            message=message, archive=archive, references=references or []
        )
        try:
            return cast(str, await self.call("send", request))
        except grpc.RpcError as error:
            raise SendFailed("Sending of the message failed: {}".format(error)) from error

    async def list(
        self,
        created_from: Optional[datetime] = None,
        created_to: Optional[datetime] = None,
        recipients: Optional[List[str]] = None,
        body_templates: Optional[List[str]] = None,
        types: Optional[List[str]] = None,
        limit: Optional[int] = None,
        references: Optional[List[Reference]] = None,
    ) -> AsyncGenerator[E, None]:
        """Send gRPC request to get list of messages.

        Args:
            created_from: Lower limit for create datetime.
            created_to: Upper limit for create datetime.
            recipients: List of recipients to filter.
            body_templates: List of body template names to filter.
            types: List of message types to filter.
            limit: Maximum number of messages returned.
            references: List of registry object references.
        """
        request = self.list_request_cls()
        if created_from:
            request.created_from.FromDatetime(created_from)
        if created_to:
            request.created_to.FromDatetime(created_to)
        if recipients:
            request.recipients[:] = recipients
        if body_templates:
            request.body_templates[:] = body_templates
        if types:
            request.types[:] = types
        if references:
            request.references.extend(self._get_messenger_references(references))
        if limit:
            request.limit = limit

        envelope_list = self.call_stream("list", request)

        async for envelope_data in envelope_list:  # pragma: no cover
            envelope = self.envelope_cls(**envelope_data)
            envelope.message._secretary_client = self.secretary_client
            yield envelope

    def _get_messenger_references(
        self, references: Sequence[Reference]
    ) -> Sequence[MessengerReference]:
        messenger_references = []
        for reference in references:
            messenger_references.append(
                MessengerReference(
                    type=reference.type,
                    value=str(reference.value),
                )
            )
        return messenger_references


class EmailMessengerClient(
    MessengerClientBase[EmailEnvelope, Email, GetEmailRequest, SendEmailRequest, ListEmailRequest]
):
    """Messenger email client."""

    stub_cls = EmailMessengerStub
    envelope_cls = EmailEnvelope
    get_request_cls = GetEmailRequest
    list_request_cls = ListEmailRequest

    def make_send_request(
        self, message: Email, *, archive: bool, references: Sequence[Reference]
    ) -> SendEmailRequest:
        """Make email send request."""
        request = SendEmailRequest()
        request.message_data.recipient = message.recipient
        request.message_data.subject_template = message.subject_template
        request.message_data.body_template = message.body_template
        request.message_data.extra_headers.update(**(message.extra_headers or {}))
        request.message_data.context.update(message.context)
        if message.sender is not None:
            request.message_data.sender = message.sender
        if message.body_template_html is not None:
            request.message_data.body_template_html = message.body_template_html
        if message.subject_template_uuid is not None:
            request.message_data.subject_template_uuid.value = str(message.subject_template_uuid)
        if message.body_template_uuid is not None:
            request.message_data.body_template_uuid.value = str(message.body_template_uuid)
        if message.body_template_html_uuid is not None:
            request.message_data.body_template_html_uuid.value = str(
                message.body_template_html_uuid
            )
        for uuid in message.attachments or ():
            request.message_data.attachments.add(value=str(uuid))
        if message.type:
            request.message_data.type = message.type
        if references:
            request.references.extend(self._get_messenger_references(references))
        request.archive = archive
        return request

    async def batch_send(self, envelopes: Iterable[EmailEnvelope]) -> AsyncGenerator[str, None]:
        """Send multiple messages as a single batch.

        Args:
            envelopes: Envelopes to be sent.

        Raises:
            SendFailed: When sending of the messages fails.
        """
        send_requests = (
            self.make_send_request(
                cast(Email, envelope.message),  # type: ignore[redundant-cast] # pydantic1
                archive=envelope.archive,
                references=envelope.references or [],
            )
            for envelope in envelopes
        )
        try:
            async for uid in self.call_stream("batch_send", send_requests):  # pragma: no cover
                yield cast(str, uid)
        except grpc.RpcError as error:
            raise SendFailed("Sending of messages failed: {}".format(error)) from error


class LetterMessengerClient(
    MessengerClientBase[
        LetterEnvelope, Letter, GetLetterRequest, SendLetterRequest, ListLetterRequest
    ]
):
    """Messenger letter client."""

    stub_cls = LetterMessengerStub
    envelope_cls = LetterEnvelope
    get_request_cls = GetLetterRequest
    list_request_cls = ListLetterRequest

    def make_send_request(
        self, message: Letter, *, archive: bool, references: Sequence[Reference]
    ) -> SendLetterRequest:
        """Make letter send request."""
        request = SendLetterRequest()
        request.message_data.recipient.CopyFrom(MessengerAddress(**message.recipient.model_dump()))
        request.message_data.file.value = message.file
        if message.type:
            request.message_data.type = message.type
        if references:
            request.references.extend(self._get_messenger_references(references))
        request.archive = archive
        return request


class SmsMessengerClient(
    MessengerClientBase[SmsEnvelope, Sms, GetSmsRequest, SendSmsRequest, ListSmsRequest]
):
    """Messenger sms client."""

    stub_cls = SmsMessengerStub
    envelope_cls = SmsEnvelope
    get_request_cls = GetSmsRequest
    list_request_cls = ListSmsRequest

    def make_send_request(
        self, message: Sms, *, archive: bool, references: Sequence[Reference]
    ) -> SendSmsRequest:
        """Make sms send request."""
        request = SendSmsRequest()
        request.message_data.recipient = message.recipient
        request.message_data.body_template = message.body_template
        request.message_data.context.update(message.context)
        if message.body_template_uuid is not None:
            request.message_data.body_template_uuid.value = str(message.body_template_uuid)
        if message.type:
            request.message_data.type = message.type
        if references:
            request.references.extend(self._get_messenger_references(references))
        request.archive = archive
        return request
