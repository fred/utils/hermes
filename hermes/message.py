"""Common classes and methods for all message types."""

import logging
from abc import ABC
from datetime import datetime
from enum import Enum, unique
from typing import ClassVar, Generic, Optional, Sequence, TypeVar, Union, cast
from uuid import UUID

from fred_types import BaseModel
from pydantic import ConfigDict, Field
from typist import SecretaryClient

from .utils import AbcAttribute

LOGGER = logging.getLogger(__name__)


@unique
class MessageStatus(int, Enum):
    """Message status."""

    PENDING = 0
    SENT = 1
    FAILED = 2
    ERROR = 3
    DELIVERED = 4
    UNDELIVERED = 5
    CANCELED = 6


class Reference(BaseModel):
    """Registry object reference."""

    type: str
    value: str


class Message(BaseModel, ABC):
    """Base class for all message types."""

    type: Optional[str] = None
    _secretary_client: Optional[SecretaryClient] = None

    render_fields: ClassVar[Sequence[str]] = AbcAttribute

    def _secretary_render(
        self, name: str, *, template: Union[UUID, str], render_method_name: str = "render"
    ) -> str:
        """Render property by django-secretary method.

        These properties are cached. The template and context attributes
        are not supposed to be overwritten.
        If you change them, you have to invalidate the cache manually.

        Args:
            name: Name of rendered property. Used for caching the results.
            template: UUID or name of template to be rendered.
            render_method_name: Name of secretary render method.

        Returns:
            Rendered template.

        Raises:
            RuntimeError: If secretary client is not set.
        """
        if not self._secretary_client:
            raise RuntimeError("Can't render {}, because secretary_client is not set".format(name))

        call = getattr(self._secretary_client, render_method_name)
        return cast(str, call(template, context=getattr(self, "context", {})))


M = TypeVar("M", bound=Message)


class MessageEnvelope(BaseModel, ABC, Generic[M]):
    """Base class for all message envelope types."""

    message: M = Field(alias="message_data")
    uid: Optional[str] = None
    create_datetime: Optional[datetime] = None
    send_datetime: Optional[datetime] = None
    delivery_datetime: Optional[datetime] = None
    attempts: int = 0
    status: Optional[MessageStatus] = None
    archive: bool = True
    references: Sequence[Reference] = []

    model_config = ConfigDict(populate_by_name=True)


E = TypeVar("E", bound=MessageEnvelope)
