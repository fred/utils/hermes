"""Hermes - a client library for a messenger."""

from .client import EmailMessengerClient, LetterMessengerClient, SmsMessengerClient
from .constants import TransportMethod
from .email import Email, EmailEnvelope
from .letter import Address, Letter, LetterEnvelope
from .message import Message, MessageEnvelope, Reference
from .sms import Sms, SmsEnvelope

__version__ = "6.0.0"

__all__ = [
    "EmailMessengerClient",
    "LetterMessengerClient",
    "SmsMessengerClient",
    "TransportMethod",
    "MessageEnvelope",
    "EmailEnvelope",
    "LetterEnvelope",
    "SmsEnvelope",
    "Message",
    "Email",
    "Letter",
    "Sms",
    "Address",
    "Reference",
]
