"""Various hermes utils."""

from typing import Any


class AbcAttributeType:
    """Abstract attribute type.

    Use instance of this class as default value of ABC
    attribute in order to enforce class attribute definition.
    """

    __isabstractmethod__ = True


# Abstract attribute singleton
AbcAttribute: Any = AbcAttributeType()
