"""Hermes constants."""

from enum import Enum, unique


@unique
class TransportMethod(str, Enum):
    """Enum with transport methods."""

    EMAIL = "email"
    LETTER = "letter"
    SMS = "sms"
