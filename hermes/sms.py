"""High level API for SMS."""

from functools import cached_property
from typing import Any, Dict, Optional
from uuid import UUID

from pydantic import computed_field

from .message import Message, MessageEnvelope


class Sms(Message):
    """Provide a structure for SMS message."""

    recipient: str
    body_template: str
    body_template_uuid: Optional[UUID] = None
    context: Dict[str, Any] = {}

    render_fields = ("body",)

    @computed_field(repr=False)  # type: ignore[prop-decorator]
    @cached_property
    def body(self) -> str:
        """Render body using django-secretary."""
        return self._secretary_render(
            "body",
            template=self.body_template_uuid or self.body_template,
            render_method_name="render",
        )


class SmsEnvelope(MessageEnvelope[Sms]):
    """Provide a structure for SMS envelope."""
