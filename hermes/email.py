"""High level API for email."""

from functools import cached_property
from typing import Any, Dict, Iterable, List, Optional
from uuid import UUID

from pydantic import computed_field, field_validator

from .message import Message, MessageEnvelope


class Email(Message):
    """Provide a structure for Email message.

    Attributes:
        recipient: Recipient(s) of the email message in RFC2822 format.
            May contain multiple email addresses.
            An iterable of strings can also be provided as input.

            Use `batch_send` instead to send individual emails to each recipient.
    """

    recipient: str
    subject_template: str
    body_template: str
    body_template_html: Optional[str] = None
    subject_template_uuid: Optional[UUID] = None
    body_template_uuid: Optional[UUID] = None
    body_template_html_uuid: Optional[UUID] = None
    sender: Optional[str] = None
    extra_headers: Dict[str, str] = {}
    context: Dict[str, Any] = {}
    attachments: List[str] = []

    @field_validator("recipient", mode="before")
    @classmethod
    def _validate_recipient(cls, value: Any) -> Any:
        """Convert recipient from iterable to str."""
        if isinstance(value, Iterable) and not isinstance(value, str):
            return ", ".join(value)
        return value

    render_fields = ("subject", "body", "body_html")

    @computed_field(repr=False)  # type: ignore[prop-decorator]
    @cached_property
    def subject(self) -> str:
        """Render subject using django-secretary."""
        return self._secretary_render(
            "subject", template=self.subject_template_uuid or self.subject_template
        )

    @computed_field(repr=False)  # type: ignore[prop-decorator]
    @cached_property
    def body(self) -> str:
        """Render body using django-secretary."""
        return self._secretary_render(
            "body", template=self.body_template_uuid or self.body_template
        )

    @computed_field(repr=False)  # type: ignore[prop-decorator]
    @cached_property
    def body_html(self) -> Optional[str]:
        """Render body_html using django-secretary."""
        template = self.body_template_html_uuid or self.body_template_html
        if template:
            return self._secretary_render(
                "body_html",
                template=template,
                render_method_name="render_html",
            )
        else:
            return None


class EmailEnvelope(MessageEnvelope[Email]):
    """Provide a structure for Email envelope."""

    message_id: Optional[str] = None
