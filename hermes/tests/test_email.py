"""Unittests for email structures."""

from typing import cast
from unittest import TestCase
from unittest.mock import Mock, call, patch, sentinel

from typist import SecretaryClient

from hermes.email import Email, EmailEnvelope

RECIPIENT = "kryton@example.com"
SUBJECT_TEMPLATE = "subject_template"
BODY_TEMPLATE = "body_template"
BODY_TEMPLATE_HTML = "body_template_html"
SENDER = "holly@example.com"
TYPE = "type"
CONTEXT = {"some": "context"}


class EmailTest(TestCase):
    """Unittests for Email structure."""

    def test_str(self):
        # Test string representation without a secretary client set.
        email = Email(
            recipient="kryten@example.org",
            subject_template="subject.txt",
            body_template="body.txt",
        )
        self.assertIn("kryten@example.org", str(email))

    def test_iterable_recipient(self):
        data = (
            ("kryten@example.org", "rimmer@example.org"),
            ["kryten@example.org", "rimmer@example.org"],
            iter(("kryten@example.org", "rimmer@example.org")),
        )
        for recipient in data:
            with self.subTest(recipient=recipient):
                email = Email(recipient=recipient, subject_template="", body_template="")  # type: ignore[arg-type]
                self.assertEqual(email.recipient, "kryten@example.org, rimmer@example.org")

    def test_render_without_secretary_client(self):
        email = Email(
            recipient="recipient@example.com",
            subject_template="subject_template",
            body_template="body_template",
            body_template_html="body_template_html",
        )
        with self.assertRaises(RuntimeError):
            email.subject
        with self.assertRaises(RuntimeError):
            email.body
        with self.assertRaises(RuntimeError):
            email.body_html


class EmailWithSecretaryClientTest(TestCase):
    """Unittests for Email structure with a secretary client."""

    def setUp(self):
        self.secretary_client = SecretaryClient("localhost")
        self.email = Email(
            recipient=RECIPIENT,
            subject_template=SUBJECT_TEMPLATE,
            body_template=BODY_TEMPLATE,
            body_template_html=BODY_TEMPLATE_HTML,
            context=CONTEXT.copy(),
            type=TYPE,
        )
        self.email._secretary_client = self.secretary_client
        patcher = patch.object(self.secretary_client, "render")
        patcher.start()
        self.addCleanup(patcher.stop)
        patcher2 = patch.object(self.secretary_client, "render_html")
        patcher2.start()
        self.addCleanup(patcher2.stop)

    def test_subject(self):
        cast(Mock, self.secretary_client).render.return_value = sentinel.subject
        self.assertEqual(self.email.subject, sentinel.subject)
        self.assertEqual(
            self.email.subject, sentinel.subject
        )  # call subject again to test the caching
        self.assertEqual(
            cast(Mock, self.secretary_client).render.mock_calls,
            [call("subject_template", context=CONTEXT)],
        )

    def test_subject_dump(self):
        # Test subject is included in model_dump.
        cast(Mock, self.secretary_client).render.return_value = sentinel.subject
        self.assertEqual(self.email.model_dump(include={"subject"}), {"subject": sentinel.subject})

    def test_body(self):
        cast(Mock, self.secretary_client).render.return_value = sentinel.body
        self.assertEqual(self.email.body, sentinel.body)
        self.assertEqual(self.email.body, sentinel.body)  # call body again to test the caching
        self.assertEqual(
            cast(Mock, self.secretary_client).render.mock_calls,
            [call("body_template", context=CONTEXT)],
        )

    def test_body_dump(self):
        # Test body is included in model_dump.
        cast(Mock, self.secretary_client).render.return_value = sentinel.body
        self.assertEqual(self.email.model_dump(include={"body"}), {"body": sentinel.body})

    def test_body_html(self):
        cast(Mock, self.secretary_client).render_html.return_value = sentinel.body_html
        self.assertEqual(self.email.body_html, sentinel.body_html)
        self.assertEqual(
            self.email.body_html, sentinel.body_html
        )  # call body_html again to test the caching
        self.assertEqual(
            cast(Mock, self.secretary_client).render_html.mock_calls,
            [call("body_template_html", context=CONTEXT)],
        )

    def test_body_html_not_set(self):
        email = Email(
            recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE, body_template=BODY_TEMPLATE
        )
        self.assertIsNone(email.body_html)

    def test_body_html_dump(self):
        # Test body_html is included in model_dump.
        cast(Mock, self.secretary_client).render_html.return_value = sentinel.body_html
        self.assertEqual(
            self.email.model_dump(include={"body_html"}), {"body_html": sentinel.body_html}
        )


class EmailEnvelopeTest(TestCase):
    """Unittests for EmailEnvelope structure."""

    def test_from_dict_data(self):
        data = {
            "message": {
                "recipient": RECIPIENT,
                "subject_template": SUBJECT_TEMPLATE,
                "body_template": BODY_TEMPLATE,
            },
            "message_id": "message_id",
        }
        email_envelope = EmailEnvelope(**data)  # type: ignore[arg-type]
        self.assertEqual(email_envelope.message_id, "message_id")
        self.assertIsInstance(email_envelope.message, Email)
        self.assertEqual(email_envelope.message.recipient, RECIPIENT)  # type: ignore # pydantic1
        self.assertEqual(email_envelope.message._secretary_client, None)
