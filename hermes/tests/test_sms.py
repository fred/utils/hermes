"""Unittests for SMS structures."""

from typing import cast
from unittest import TestCase
from unittest.mock import Mock, call, patch, sentinel

from typist import SecretaryClient

from hermes.sms import Sms

RECIPIENT = "+420111222333"
BODY_TEMPLATE = "body_template"
TYPE = "type"


class SmsTest(TestCase):
    def test_str(self):
        # Test string representation without a secretary client set.
        sms = Sms(recipient="+12345", body_template="body.txt")
        self.assertIn("+12345", str(sms))


class SmsWithSecretaryClientTest(TestCase):
    """Unittests for Sms structure with a secretary client."""

    def setUp(self):
        self.secretary_client = SecretaryClient("localhost")
        patcher = patch.object(self.secretary_client, "render")
        patcher.start()
        self.addCleanup(patcher.stop)
        self.sms = Sms(recipient=RECIPIENT, body_template=BODY_TEMPLATE, context={}, type=TYPE)
        self.sms._secretary_client = self.secretary_client

    def test_body(self):
        cast(Mock, self.secretary_client).render.return_value = sentinel.body
        self.assertEqual(self.sms.body, sentinel.body)
        self.assertEqual(self.sms.body, sentinel.body)  # call body again to test the caching
        self.assertEqual(
            cast(Mock, self.secretary_client).render.mock_calls,
            [call("body_template", context={})],
        )

    def test_body_dump(self):
        # Test body is included in model_dump.
        cast(Mock, self.secretary_client).render.return_value = sentinel.body
        self.assertEqual(self.sms.model_dump(include={"body"}), {"body": sentinel.body})
