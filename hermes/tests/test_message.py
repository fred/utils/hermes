"""Unittests for generic message structures."""

from typing import cast
from unittest import TestCase
from unittest.mock import Mock, call, patch, sentinel

from typist import SecretaryClient

from hermes.message import Message


class TestMessage(Message):
    """Test message."""

    data: str
    render_fields = ("subject",)

    @property
    def subject(self) -> str:
        return self._secretary_render("subject", template="template.txt")


class MessageTest(TestCase):
    """Unittests for message."""

    def test_secretary_render(self):
        """Test _secretary_render without secretary client."""
        message = TestMessage(data="gazpacho")
        with self.assertRaises(RuntimeError):
            message._secretary_render(
                sentinel.name,
                template=sentinel.template,
                render_method_name=sentinel.render_method_name,
            )


class MessageWithSecretaryClientTest(TestCase):
    """Unittests for message structure with a secretary client."""

    def setUp(self):
        self.secretary_client = SecretaryClient("localhost")
        patcher = patch.object(self.secretary_client, "render")
        self.addCleanup(patcher.stop)
        patcher.start()
        self.message = TestMessage(data="gazpacho", type="T")
        self.message._secretary_client = self.secretary_client

    def test_secretary_render(self):
        """Test _secretary_render with secretary client."""
        cast(Mock, self.secretary_client).render.return_value = sentinel.subject
        self.assertEqual(self.message.subject, sentinel.subject)
        self.assertEqual(
            cast(Mock, self.secretary_client).render.mock_calls, [call("template.txt", context={})]
        )
