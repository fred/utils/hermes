"""Unittests for MessengerClient."""

from datetime import datetime
from typing import Any, AsyncGenerator, Iterable, Type, TypeVar, Union, cast
from unittest import TestCase
from unittest.mock import call, sentinel
from uuid import UUID

import grpc
from aioitertools import iter as aiter, list as alist
from fred_api.messenger.common_types_pb2 import Reference as MessengerReference, Uuid
from fred_api.messenger.email_pb2 import Email as MessengerEmail
from fred_api.messenger.letter_pb2 import Address as MessengerAddress, Letter as MessengerLetter
from fred_api.messenger.service_email_grpc_pb2 import (
    GetEmailReply,
    ListEmailReply,
    SendEmailReply,
    SendEmailRequest,
)
from fred_api.messenger.service_letter_grpc_pb2 import (
    GetLetterReply,
    ListLetterReply,
    SendLetterReply,
    SendLetterRequest,
)
from fred_api.messenger.service_sms_grpc_pb2 import (
    GetSmsReply,
    ListSmsReply,
    SendSmsReply,
    SendSmsRequest,
)
from fred_api.messenger.sms_pb2 import Sms as MessengerSms
from frgal.utils import AsyncTestClientMixin, make_awaitable
from google.protobuf.message import Message as GrpcMessage
from grpc import StatusCode
from grpc._channel import _RPCState, _SingleThreadedRendezvous as _Rendezvous
from typist import SecretaryClient

from hermes.client import (
    EmailMessengerClient,
    LetterMessengerClient,
    MessengerClientBase,
    MessengerDecoder,
    NoMessage,
    SendFailed,
    SmsMessengerClient,
)
from hermes.email import Email, EmailEnvelope
from hermes.letter import Address, Letter
from hermes.message import Message, MessageStatus, Reference
from hermes.sms import Sms

try:
    from unittest import IsolatedAsyncioTestCase  # type: ignore
except ImportError:  # pragma: no cover
    from asynctest import TestCase as IsolatedAsyncioTestCase  # type: ignore


T = TypeVar("T")


async def async_gen(iterable: Iterable[T]) -> AsyncGenerator[T, None]:
    """Convert iterable to async generator."""
    for value in iterable:
        yield value


class TestEmailMessengerClient(AsyncTestClientMixin, EmailMessengerClient):
    """Testing version of a EmailMessengerClient."""

    exhaust_iterables = True


class TestLetterMessengerClient(AsyncTestClientMixin, LetterMessengerClient):
    """Testing version of a LetterMessengerClient."""


class TestSmsMessengerClient(AsyncTestClientMixin, SmsMessengerClient):
    """Testing version of a SmsMessengerClient."""


class MessengerClientTestMixin:
    """Unittests for MessengerClient."""

    client_cls: Type[MessengerClientBase]
    get_message_reply_type: Union[GetEmailReply, GetLetterReply, GetSmsReply]
    list_messages_reply_type: Union[ListEmailReply, ListLetterReply, ListSmsReply]
    message_cls: Type[Message]
    grpc_service_path: str
    has_subject_template = True
    has_body_template = True
    recipient: Any = "recipient"
    recipient_message: GrpcMessage

    def setUp(self):
        self.secretary_client = SecretaryClient("localhost")
        self.client = self.client_cls(sentinel.netloc, secretary_client=self.secretary_client)

    async def test_get_message(self):
        reply = self.get_message_reply_type()
        reply.data.envelope.uid.value = "some uid"
        reply.data.envelope.create_datetime.FromDatetime(datetime(2000, 1, 1, 15, 0, 0))
        if isinstance(self.recipient, str):
            reply.data.envelope.message_data.recipient = self.recipient
        else:
            reply.data.envelope.message_data.recipient.CopyFrom(self.recipient_message)
        if self.has_subject_template:
            reply.data.envelope.message_data.subject_template = "subject_template"
        if self.has_body_template:
            reply.data.envelope.message_data.body_template = "body_template"
        reply.data.envelope.status = MessageStatus.PENDING.value
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        result = await self.client.get("some uid")

        cast(TestCase, self).assertIsInstance(result, self.client.envelope_cls)
        cast(TestCase, self).assertEqual(result.uid, "some uid")
        cast(TestCase, self).assertEqual(result.message.recipient, self.recipient)
        cast(TestCase, self).assertEqual(result.status, MessageStatus.PENDING)

        request = self.client.get_request_cls()
        request.uid.value = "some uid"
        cast(TestCase, self).assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="{}/get".format(self.grpc_service_path), timeout=None)],
        )

    async def test_get_message_error(self):
        error = _Rendezvous(_RPCState((), "", "", StatusCode.UNKNOWN, ""), None, None, None)
        cast(AsyncTestClientMixin, self.client).mock.side_effect = error
        with cast(TestCase, self).assertRaises(grpc.RpcError):
            await self.client.get("some uid")

    async def test_get_message_not_found(self):
        error = _Rendezvous(_RPCState((), "", "", StatusCode.NOT_FOUND, ""), None, None, None)
        cast(AsyncTestClientMixin, self.client).mock.side_effect = error
        with cast(TestCase, self).assertRaises(NoMessage):
            await self.client.get("some uid")

    async def test_list_message_empty_request(self):
        reply = self.list_messages_reply_type()
        reply.data.envelope.uid.value = "some uid"
        reply.data.envelope.create_datetime.FromDatetime(datetime(2000, 1, 1, 15, 0, 0))
        if isinstance(self.recipient, str):
            reply.data.envelope.message_data.recipient = self.recipient
        else:
            reply.data.envelope.message_data.recipient.CopyFrom(self.recipient_message)
        if self.has_subject_template:
            reply.data.envelope.message_data.subject_template = "subject_template"
        if self.has_body_template:
            reply.data.envelope.message_data.body_template = "body_template"
        cast(AsyncTestClientMixin, self.client).mock.return_value = async_gen([reply])

        result = [m async for m in self.client.list()]

        cast(TestCase, self).assertEqual(len(result), 1)
        cast(TestCase, self).assertIsInstance(result[0], self.client.envelope_cls)
        cast(TestCase, self).assertEqual(result[0].message.recipient, self.recipient)
        cast(TestCase, self).assertEqual(
            result[0].message._secretary_client, self.secretary_client
        )

        request = self.client.list_request_cls()
        cast(TestCase, self).assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="{}/list".format(self.grpc_service_path), timeout=None)],
        )

    async def test_list_message_not_empty_request(self):
        reply = self.list_messages_reply_type()
        reply.data.envelope.uid.value = "some uid"
        reply.data.envelope.create_datetime.FromDatetime(datetime(2000, 1, 1, 15, 0, 0))
        if isinstance(self.recipient, str):
            reply.data.envelope.message_data.recipient = self.recipient
        else:
            reply.data.envelope.message_data.recipient.CopyFrom(self.recipient_message)
        if self.has_subject_template:
            reply.data.envelope.message_data.subject_template = "subject_template"
        if self.has_body_template:
            reply.data.envelope.message_data.body_template = "body_template"
        cast(AsyncTestClientMixin, self.client).mock.return_value = async_gen([reply])

        result = [
            m
            async for m in self.client.list(
                created_from=datetime(2000, 1, 1),
                created_to=datetime(2000, 1, 2),
                recipients=["recipient"],
                types=["techcheck"],
                limit=10,
                references=[
                    Reference(type="registrar", value="REG-NIC"),
                    Reference(type="domain", value="142"),
                ],
                body_templates=["body_template", "body_template2"]
                if self.has_body_template
                else None,
            )
        ]

        cast(TestCase, self).assertEqual(len(result), 1)
        cast(TestCase, self).assertIsInstance(result[0], self.client.envelope_cls)
        cast(TestCase, self).assertEqual(result[0].message.recipient, self.recipient)
        cast(TestCase, self).assertEqual(
            result[0].message._secretary_client, self.secretary_client
        )

        request = self.client.list_request_cls()
        request.created_from.FromDatetime(datetime(2000, 1, 1))
        request.created_to.FromDatetime(datetime(2000, 1, 2))
        request.recipients[:] = ["recipient"]
        if self.has_body_template:
            request.body_templates[:] = ["body_template", "body_template2"]
        request.types[:] = ["techcheck"]
        request.limit = 10
        request.references.extend(
            [
                MessengerReference(type="registrar", value="REG-NIC"),
                MessengerReference(type="domain", value="142"),
            ]
        )
        cast(TestCase, self).assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="{}/list".format(self.grpc_service_path), timeout=None)],
        )


class EmailMessengerClientTest(MessengerClientTestMixin, IsolatedAsyncioTestCase):
    """Unittests for EmailMessengerClient."""

    client_cls = TestEmailMessengerClient
    get_message_reply_type = GetEmailReply
    list_messages_reply_type = ListEmailReply
    message_cls = Email
    grpc_service_path = "/Fred.Messenger.Api.Email.EmailMessenger"

    async def test_send_default(self):
        reply = SendEmailReply()
        reply.data.uid.value = "42"
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        message = Email(
            recipient="test@example.com",
            subject_template="subject_template",
            body_template="body_template",
        )
        result = await self.client.send(message)

        self.assertEqual(result, "42")

        email = MessengerEmail()
        email.recipient = "test@example.com"
        email.subject_template = "subject_template"
        email.body_template = "body_template"
        request = SendEmailRequest(message_data=email, archive=True)
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="/Fred.Messenger.Api.Email.EmailMessenger/send", timeout=None)],
        )

    async def test_send_type(self):
        reply = SendEmailReply()
        reply.data.uid.value = "42"
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        message = Email(
            recipient="test@example.com",
            subject_template="subject_template",
            body_template="body_template",
            type="techcheck",
        )
        result = await self.client.send(message)

        self.assertEqual(result, "42")

        email = MessengerEmail()
        email.recipient = "test@example.com"
        email.subject_template = "subject_template"
        email.body_template = "body_template"
        email.type = "techcheck"
        request = SendEmailRequest(message_data=email, archive=True)
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="/Fred.Messenger.Api.Email.EmailMessenger/send", timeout=None)],
        )

    async def test_send_extra_headers(self):
        reply = SendEmailReply()
        reply.data.uid.value = "42"
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        message = Email(
            recipient="test@example.com",
            subject_template="subject_template",
            body_template="body_template",
            extra_headers={"header": "value"},
        )
        result = await self.client.send(message)

        self.assertEqual(result, "42")

        email = MessengerEmail()
        email.recipient = "test@example.com"
        email.subject_template = "subject_template"
        email.body_template = "body_template"
        email.extra_headers["header"] = "value"
        request = SendEmailRequest(message_data=email, archive=True)
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="/Fred.Messenger.Api.Email.EmailMessenger/send", timeout=None)],
        )

    async def test_send_context(self):
        reply = SendEmailReply()
        reply.data.uid.value = "42"
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        context = {"test": "test"}

        message = Email(
            recipient="test@example.com",
            subject_template="subject_template",
            body_template="body_template",
            context=context,
        )
        result = await self.client.send(message)

        self.assertEqual(result, "42")

        email = MessengerEmail()
        email.recipient = "test@example.com"
        email.subject_template = "subject_template"
        email.body_template = "body_template"
        email.context.update({"test": "test"})
        request = SendEmailRequest(message_data=email, archive=True)
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="/Fred.Messenger.Api.Email.EmailMessenger/send", timeout=None)],
        )

    async def test_send_sender(self):
        reply = SendEmailReply()
        reply.data.uid.value = "42"
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        message = Email(
            recipient="test@example.com",
            subject_template="subject_template",
            body_template="body_template",
            sender="sender@example.com",
        )
        result = await self.client.send(message)

        self.assertEqual(result, "42")

        email = MessengerEmail()
        email.recipient = "test@example.com"
        email.sender = "sender@example.com"
        email.subject_template = "subject_template"
        email.body_template = "body_template"
        request = SendEmailRequest(message_data=email, archive=True)
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="/Fred.Messenger.Api.Email.EmailMessenger/send", timeout=None)],
        )

    async def test_send_body_template_html(self):
        reply = SendEmailReply()
        reply.data.uid.value = "42"
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        message = Email(
            recipient="test@example.com",
            subject_template="subject_template",
            body_template="body_template",
            body_template_html="html_body_template",
        )
        result = await self.client.send(message)

        self.assertEqual(result, "42")

        email = MessengerEmail()
        email.recipient = "test@example.com"
        email.body_template_html = "html_body_template"
        email.subject_template = "subject_template"
        email.body_template = "body_template"
        request = SendEmailRequest(message_data=email, archive=True)
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="/Fred.Messenger.Api.Email.EmailMessenger/send", timeout=None)],
        )

    async def test_send_template_uuid(self):
        reply = SendEmailReply()
        reply.data.uid.value = "42"
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        message = Email(
            recipient="test@example.com",
            subject_template="subject_template",
            body_template="body_template",
            body_template_html="html_body_template",
            subject_template_uuid=UUID(int=1),
            body_template_uuid=UUID(int=2),
            body_template_html_uuid=UUID(int=3),
        )
        result = await self.client.send(message)

        self.assertEqual(result, "42")

        email = MessengerEmail()
        email.recipient = "test@example.com"
        email.body_template_html = "html_body_template"
        email.subject_template = "subject_template"
        email.body_template = "body_template"
        email.subject_template_uuid.value = str(UUID(int=1))
        email.body_template_uuid.value = str(UUID(int=2))
        email.body_template_html_uuid.value = str(UUID(int=3))
        request = SendEmailRequest(message_data=email, archive=True)
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="/Fred.Messenger.Api.Email.EmailMessenger/send", timeout=None)],
        )

    async def test_send_archive(self):
        reply = SendEmailReply()
        reply.data.uid.value = "42"
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        message = Email(
            recipient="test@example.com",
            subject_template="subject_template",
            body_template="body_template",
        )
        result = await self.client.send(message, archive=True)

        self.assertEqual(result, "42")

        email = MessengerEmail()
        email.recipient = "test@example.com"
        email.subject_template = "subject_template"
        email.body_template = "body_template"
        request = SendEmailRequest(message_data=email, archive=True)
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="/Fred.Messenger.Api.Email.EmailMessenger/send", timeout=None)],
        )

    async def test_send_no_archive(self):
        reply = SendEmailReply()
        reply.data.uid.value = "42"
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        message = Email(
            recipient="test@example.com",
            subject_template="subject_template",
            body_template="body_template",
        )
        result = await self.client.send(message, archive=False)

        self.assertEqual(result, "42")

        email = MessengerEmail()
        email.recipient = "test@example.com"
        email.subject_template = "subject_template"
        email.body_template = "body_template"
        request = SendEmailRequest(message_data=email, archive=False)
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="/Fred.Messenger.Api.Email.EmailMessenger/send", timeout=None)],
        )

    async def test_send_attachments(self):
        reply = SendEmailReply()
        reply.data.uid.value = "42"
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        message = Email(
            recipient="test@example.com",
            subject_template="subject_template",
            body_template="body_template",
            attachments=["15", "16"],
        )
        result = await self.client.send(message)

        self.assertEqual(result, "42")

        email = MessengerEmail()
        email.recipient = "test@example.com"
        email.subject_template = "subject_template"
        email.body_template = "body_template"
        email.attachments.add(value="15")
        email.attachments.add(value="16")
        request = SendEmailRequest(message_data=email, archive=True)
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="/Fred.Messenger.Api.Email.EmailMessenger/send", timeout=None)],
        )

    async def test_send_references(self):
        reply = SendEmailReply()
        reply.data.uid.value = "42"
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        message = Email(
            recipient="test@example.com",
            subject_template="subject_template",
            body_template="body_template",
        )
        result = await self.client.send(
            message, references=[Reference(type="registrar", value="REG-NIC")]
        )

        self.assertEqual(result, "42")

        email = MessengerEmail()
        email.recipient = "test@example.com"
        email.subject_template = "subject_template"
        email.body_template = "body_template"
        request = SendEmailRequest(message_data=email, archive=True)
        request.references.extend(
            [
                MessengerReference(type="registrar", value="REG-NIC"),
            ]
        )
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="/Fred.Messenger.Api.Email.EmailMessenger/send", timeout=None)],
        )

    async def test_send_failed(self):
        message = Email(
            recipient="test@example.com",
            subject_template="subject_template",
            body_template="body_template",
            attachments=["15", "16"],
        )
        error = _Rendezvous(_RPCState((), "", "", StatusCode.UNKNOWN, ""), None, None, None)
        cast(AsyncTestClientMixin, self.client).mock.side_effect = error
        with self.assertRaises(SendFailed):
            await self.client.send(message)

    async def test_batch_send(self):
        envelope = EmailEnvelope(  # type: ignore[call-arg]
            message=Email(
                recipient="test@example.com",
                subject_template="subject_template",
                body_template="body_template",
            ),
        )
        reply1 = SendEmailReply()
        reply1.data.uid.value = "UID1"
        reply2 = SendEmailReply()
        reply2.data.uid.value = "UID2"
        cast(AsyncTestClientMixin, self.client).mock.return_value = aiter([reply1, reply2])

        self.assertEqual(
            await alist(cast(EmailMessengerClient, self.client).batch_send([envelope, envelope])),
            ["UID1", "UID2"],
        )

        email = MessengerEmail()
        email.recipient = "test@example.com"
        email.subject_template = "subject_template"
        email.body_template = "body_template"
        request = (
            SendEmailRequest(message_data=email, archive=True),
            SendEmailRequest(message_data=email, archive=True),
        )
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [
                call(
                    request,
                    method="/Fred.Messenger.Api.Email.EmailMessenger/batch_send",
                    timeout=None,
                )
            ],
        )

    async def test_batch_send_references(self):
        envelope = EmailEnvelope(  # type: ignore[call-arg]
            message=Email(
                recipient="test@example.com",
                subject_template="subject_template",
                body_template="body_template",
                type="TYPE",
            ),
            references=[Reference(type="registrar", value="REG-NIC")],
        )
        reply = SendEmailReply()
        reply.data.uid.value = "UID1"
        cast(AsyncTestClientMixin, self.client).mock.return_value = aiter([reply])

        self.assertEqual(
            await alist(cast(EmailMessengerClient, self.client).batch_send([envelope])),
            ["UID1"],
        )

        email = MessengerEmail()
        email.recipient = "test@example.com"
        email.subject_template = "subject_template"
        email.body_template = "body_template"
        email.type = "TYPE"
        request = SendEmailRequest(message_data=email, archive=True)
        request.references.append(MessengerReference(type="registrar", value="REG-NIC"))
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [
                call(
                    (request,),
                    method="/Fred.Messenger.Api.Email.EmailMessenger/batch_send",
                    timeout=None,
                )
            ],
        )

    async def test_batch_send_failed(self):
        message = Email(
            recipient="test@example.com",
            subject_template="subject_template",
            body_template="body_template",
        )
        error = _Rendezvous(_RPCState((), "", "", StatusCode.UNKNOWN, ""), None, None, None)
        cast(AsyncTestClientMixin, self.client).mock.side_effect = error
        with self.assertRaises(SendFailed):
            await alist(
                cast(EmailMessengerClient, self.client).batch_send(
                    [EmailEnvelope(message=message)]  # type: ignore[call-arg]
                )
            )


class LetterMessengerClientTest(MessengerClientTestMixin, IsolatedAsyncioTestCase):
    """Unittests for LetterMessengerClient."""

    client_cls = TestLetterMessengerClient
    get_message_reply_type = GetLetterReply
    list_messages_reply_type = ListLetterReply
    message_cls = Letter
    grpc_service_path = "/Fred.Messenger.Api.Letter.LetterMessenger"
    has_subject_template = False
    has_body_template = False
    recipient = Address(
        name="Kryton",
        organization="Jupiter Mining Corp.",
        city="Red Dwarf",
        postal_code="JMC",
        country="JU",
    )
    recipient_message = MessengerAddress(
        name="Kryton",
        organization="Jupiter Mining Corp.",
        city="Red Dwarf",
        postal_code="JMC",
        country="JU",
    )

    async def test_send_default(self):
        reply = SendLetterReply()
        reply.data.uid.value = "42"
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        message = Letter(recipient=self.recipient, file="FILE")
        result = await self.client.send(message)

        self.assertEqual(result, "42")

        letter = MessengerLetter()
        letter.recipient.CopyFrom(self.recipient_message)
        letter.file.value = "FILE"
        request = SendLetterRequest(message_data=letter, archive=True)
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="{}/send".format(self.grpc_service_path), timeout=None)],
        )

    async def test_send_full(self):
        reply = SendLetterReply()
        reply.data.uid.value = "42"
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        message = Letter(recipient=self.recipient, file="FILE", type="techcheck")
        result = await self.client.send(
            message, archive=False, references=[Reference(type="registrar", value="REG-NIC")]
        )

        self.assertEqual(result, "42")

        letter = MessengerLetter()
        letter.recipient.CopyFrom(self.recipient_message)
        letter.file.value = "FILE"
        letter.type = "techcheck"
        request = SendLetterRequest(message_data=letter, archive=False)
        request.references.extend(
            [
                MessengerReference(type="registrar", value="REG-NIC"),
            ]
        )
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="{}/send".format(self.grpc_service_path), timeout=None)],
        )

    async def test_send_failed(self):
        message = Letter(recipient=self.recipient, file="FILE")
        error = _Rendezvous(_RPCState((), "", "", StatusCode.UNKNOWN, ""), None, None, None)
        cast(AsyncTestClientMixin, self.client).mock.side_effect = error
        with self.assertRaises(SendFailed):
            await self.client.send(message)

    async def test_get_message_with_file(self):
        reply = GetLetterReply()
        reply.data.envelope.message_data.recipient.CopyFrom(self.recipient_message)
        reply.data.envelope.message_data.file.value = str(UUID(int=42))
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        result = await self.client.get("some uid")

        cast(TestCase, self).assertEqual(result.message.file, str(UUID(int=42)))


class SmsMessengerClientTest(MessengerClientTestMixin, IsolatedAsyncioTestCase):
    """Unittests for SmsMessengerClient."""

    client_cls = TestSmsMessengerClient
    get_message_reply_type = GetSmsReply
    list_messages_reply_type = ListSmsReply
    message_cls = Sms
    grpc_service_path = "/Fred.Messenger.Api.Sms.SmsMessenger"
    has_subject_template = False

    async def test_send_default(self):
        reply = SendSmsReply()
        reply.data.uid.value = "42"
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        message = Sms(recipient="+420.999888777", body_template="body_template")
        result = await self.client.send(message)

        self.assertEqual(result, "42")

        sms = MessengerSms()
        sms.recipient = "+420.999888777"
        sms.body_template = "body_template"
        request = SendSmsRequest(message_data=sms, archive=True)
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="{}/send".format(self.grpc_service_path), timeout=None)],
        )

    async def test_send_full(self):
        reply = SendSmsReply()
        reply.data.uid.value = "42"
        cast(AsyncTestClientMixin, self.client).mock.return_value = make_awaitable(reply)

        message = Sms(
            recipient="+420.999888777",
            body_template="body_template",
            type="techcheck",
            body_template_uuid=UUID(int=1),
        )
        result = await self.client.send(
            message, archive=False, references=[Reference(type="registrar", value="REG-NIC")]
        )

        self.assertEqual(result, "42")

        sms = MessengerSms()
        sms.recipient = "+420.999888777"
        sms.body_template = "body_template"
        sms.type = "techcheck"
        sms.body_template_uuid.value = str(UUID(int=1))
        request = SendSmsRequest(message_data=sms, archive=False)
        request.references.extend(
            [
                MessengerReference(type="registrar", value="REG-NIC"),
            ]
        )
        self.assertEqual(
            cast(AsyncTestClientMixin, self.client).mock.mock_calls,
            [call(request, method="{}/send".format(self.grpc_service_path), timeout=None)],
        )

    async def test_send_failed(self):
        message = Sms(recipient="+420.999888777", body_template="body_template")
        error = _Rendezvous(_RPCState((), "", "", StatusCode.UNKNOWN, ""), None, None, None)
        cast(AsyncTestClientMixin, self.client).mock.side_effect = error
        with self.assertRaises(SendFailed):
            await self.client.send(message)


class MessengerDecoderTest(TestCase):
    """Unittests for MessengerDecoder."""

    def setUp(self):
        self.decoder = MessengerDecoder()

    def test_decode_email(self):
        email = MessengerEmail()
        email.attachments.add(value="file:1")
        email.attachments.add(value="file:2")
        self.assertEqual(self.decoder.decode(email)["attachments"], ["file:1", "file:2"])

    def test_decode_letter(self):
        letter = MessengerLetter()
        letter.recipient.name = "Kryton"
        letter.file.value = "42"
        self.assertEqual(
            self.decoder.decode(letter),
            {
                "recipient": Address(name="Kryton").model_dump(),
                "file": "42",
                "type": None,
                "body_template": None,
                "body_template_uuid": None,
                "context": {},
            },
        )

    def test_decode_reference(self):
        reference_message = MessengerReference(type="domain", value="nic.cz")
        reference = Reference(type="domain", value="nic.cz")
        self.assertEqual(self.decoder.decode(reference_message), reference)

    def test_decode_uuid(self):
        self.assertEqual(self.decoder.decode(Uuid(value=None)), None)
        self.assertEqual(self.decoder.decode(Uuid(value="")), None)
        self.assertEqual(self.decoder.decode(Uuid(value=str(UUID(int=42)))), UUID(int=42))
